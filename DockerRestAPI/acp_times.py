"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

import logging

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

controls = [
   [0, 200, 15, 34],
   [200, 400, 15, 32],
   [400, 600, 15, 30],
   [600, 1000, 11.428, 28],
   [1000, 1300, 13.333, 26]
]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An english formatted Date and Time string indicating the control open time.
      This will be in the same time zone as the brevet start time.
   """
   arrowstart = arrow.get(brevet_start_time)
   
   control = accurate_bracket(control_dist_km, brevet_dist_km)
   
   offset = control_dist_km / control[3]
   
   return arrowstart.shift(hours = offset).format('dddd MM/DD/YYYY HH:mm')


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An english formatted Date and Time string indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """
   arrowstart = arrow.get(brevet_start_time)

   if(control_dist_km == 0):
      return arrowstart.shift(hours = 1).isoformat()
   control = accurate_bracket(control_dist_km, brevet_dist_km)

   offset = control_dist_km / control[2]
   
   return arrowstart.shift(hours = offset).format('dddd MM/DD/YYYY HH:mm')

def accurate_bracket(control_dist_km, brevet_dist_km):
   if control_dist_km >= brevet_dist_km:
      for control in controls:
         if control[1] == brevet_dist_km:
            return control

   for control in controls:
      if control_dist_km >= control[0] and control_dist_km < control[1]:
         return control

