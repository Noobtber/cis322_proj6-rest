import os
from flask import Flask, redirect, url_for, request, render_template
import flask
from flask_restful import Resource, Api
from pymongo import MongoClient
import pymongo
import arrow  # Replacement for datetime, based on moment.js
import acp_times_frontend  # Brevet time calculations
import acp_times_frontend
#import pandas
import bson.json_util

app = Flask(__name__)
api = Api(app)

client = MongoClient('mongodb://db:27017/')
db = client.controls

@app.route('/')
def controls():
    _items = db.controls.find()
    items = [item for item in _items]

    return render_template('calculator.html', items=items)

@app.route('/results')
def results():
    _items = db.controls.find()
    items = [item for item in _items]
    app.logger.info(len(items))
    if len(items) == 0:
        return render_template('empty.html')
    else:
        return render_template('results.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    if request.form['km'] != '':
        startdatetime = arrow.get(str(request.form['begin_date']) + 'T' + str(request.form['begin_time']) + "-08:00")
        item_doc = {
            'distance': request.form['distance'],
            'location': request.form['km'],
            'locationname': request.form['location'],
            'open': acp_times_frontend.open_time(float(request.form['km']), float(request.form['distance']), startdatetime),
            'close': acp_times_frontend.open_time(float(request.form['km']), float(request.form['distance']), startdatetime)
        }
        db.controls.insert_one(item_doc)

    return redirect(url_for('controls'))

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', type=float) # Removed restriction to allow for higher input
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brevetdist = request.args.get('distance', type=float)

    startdatetime = arrow.get(request.args.get('begin_date', "2017-01-01", type=str) + 'T' + request.args.get('begin_time', "00:00", type=str) + "-08:00")

    open_time = acp_times_frontend.open_time(km, brevetdist, startdatetime)
    close_time = acp_times_frontend.close_time(km, brevetdist, startdatetime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)
@app.route("/listAll", defaults={'type':'json'})
@app.route("/listAll/<path:type>")
def all(type):
    # return {
    #         'Laptops': ['Mac OS', 'Dell', 
    #         'Windozzee',
	#     'Yet another laptop!',
	#     'Yet yet another laptop!'
    #         ]
    #     }
    top = request.args.get('top', default = 0, type = int)
    controls = db.controls.find().sort('location', pymongo.ASCENDING)

    return bson.json_util.dumps(controls).replace('[', '').replace(']', '')
    if type == "csv":
        pass
    else:
        pass
    pass

@app.route("/listOpenOnly/")
@app.route("/listOpenOnly/<path:type>")
def openOnly(type):
    top = request.args.get('top', default = 0, type = int)

    if type == "csv":
        pass
    else:
        pass
    pass

@app.route("/listCloseOnly/<path:type>")
def closeOnly(type):
    top = request.args.get('top', default = 0, type = int)
    if type == "csv":
        pass
    else:
        pass
    pass

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

api.add_resource(Laptop, '/laptop')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='80', debug=True)
